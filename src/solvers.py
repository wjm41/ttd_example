from abc import ABC, abstractmethod
import numpy as np

class BaseSolver(ABC):
    def __init__(self,func):
        self.function_to_solve = func

    def _evaluate(self, *args):
        value = self.function_to_solve(*args)
        return value
    
    @abstractmethod
    def return_root(self,):
        pass
    
class BisectionSolver(BaseSolver):
    def __init__(self,func, lower_bound:float, upper_bound:float):
        super().__init__(func)
        self.lower_bound = lower_bound
        self.upper_bound = upper_bound
    def _returnMidpoint(self,):
        midpoint = (self.upper_bound + self.lower_bound)/2
        return midpoint
    
    def _adjustBounds(self, val_at_midpoint, val_at_lower_bound, midpoint):
        midpoint_sign = -1 if val_at_midpoint < 0 else 1
        lower_bound_sign = -1 if val_at_lower_bound < 0 else 1
        if midpoint_sign == lower_bound_sign:
            self.lower_bound = midpoint
        else:
            self.upper_bound = midpoint
    
    def return_root(self, n_iter:int, eta:float=1e-3):
        for _ in range(n_iter):
            midpoint = self._returnMidpoint()
            value_at_midpoint = self._evaluate(midpoint)
            if abs(value_at_midpoint) < eta:
                return midpoint
            else:
                val_at_lower_bound = self._evaluate(self.lower_bound)
                self._adjustBounds(value_at_midpoint, val_at_lower_bound, midpoint)
        return self._returnMidpoint()