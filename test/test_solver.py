from unittest import TestCase
from src.solvers import BaseSolver, BisectionSolver

class TestAbstractSolvers(TestCase):
    def testEvaluateFunction(self):
        func = lambda x: x
        solver = BaseSolver(func)
        self.assertEqual(solver._evaluate(5), 5)
        
        func = lambda x: x**2
        solver = BaseSolver(func)
        self.assertEqual(solver._evaluate(5), 25)
        
        func = lambda x: 1/x
        solver = BaseSolver(func)
        with self.assertRaises(ZeroDivisionError):
            solver._evaluate(0)

class TestBisectionSolver(TestCase):
    def testReturnRoot(self):
        
        func = lambda x: x-0.5
        solver = BisectionSolver(func=func, lower_bound=0, upper_bound=1)
        self.assertEqual(solver.return_root(n_iter=10), 0.5)
        
        func = lambda x: x**2-4
        solver = BisectionSolver(func=func, lower_bound=0, upper_bound=1)
        self.assertAlmostEqual(solver.return_root(n_iter=10),1, 2)
        
        func = lambda x: x**2-4
        solver = BisectionSolver(func=func, lower_bound=0, upper_bound=5)
        self.assertAlmostEqual(solver.return_root(n_iter=10),2, 3)
        
        func = lambda x: (x-4)**2-4
        solver = BisectionSolver(func=func, lower_bound=0, upper_bound=5)
        self.assertAlmostEqual(solver.return_root(n_iter=100),2, 3)
        
        pass
    def testFindMidpoint(self):
        func = lambda x: 1
        solver = BisectionSolver(func=func, lower_bound=0, upper_bound=1)
        self.assertEqual(solver._returnMidpoint(), 0.5)

        solver.lower_bound = 0.5
        self.assertEqual(solver._returnMidpoint(), 0.75)
        
    def testAdjustsBounds(self):
        func = lambda x: 0
        solver = BisectionSolver(func=func, lower_bound=0, upper_bound=1)
        
        mid_point_value = -1
        lower_bound_value = -1
        solver._adjustBounds(mid_point_value, lower_bound_value, 0.5)
        self.assertEqual(solver.lower_bound, 0.5)
        self.assertEqual(solver.upper_bound, 1)
        
        mid_point_value = -1
        lower_bound_value = +1
        solver._adjustBounds(mid_point_value, lower_bound_value, 0.75)
        self.assertEqual(solver.lower_bound, 0.5)
        self.assertEqual(solver.upper_bound, 0.75)
        
        mid_point_value = -1
        lower_bound_value = -1
        solver._adjustBounds(mid_point_value,lower_bound_value, 0.625)
        self.assertEqual(solver.lower_bound, 0.625)
        self.assertEqual(solver.upper_bound, 0.75)
        
        mid_point_value = +1
        lower_bound_value = -1
        solver._adjustBounds(mid_point_value,lower_bound_value, 0.999)
        self.assertEqual(solver.lower_bound, 0.625)
        self.assertEqual(solver.upper_bound, 0.999)